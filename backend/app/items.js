const express = require('express');
const Item = require('../models/Item');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');
const Category = require('../models/Category');
const User = require('../models/User');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = () => {
    const router = express.Router();

    router.get('/', (req, res) => {

        if (req.query.category) {
            Category.find({name: req.query.category}).then(result => {
                Item.find({category: result[0]._id}).populate('category user')
                    .then(items => res.send(items))
                    .catch(() => res.sendStatus(500));
            }).catch(() => res.sendStatus(500));
        } else {
            Item.find().populate('category user')
                .then(result => res.send(result))
                .catch(() => res.sendStatus(500));
        }

    });

    router.post('/', upload.single('image'), async (req, res) => {
        const token = req.get('Token');
        const itemData = req.body;

        if (!token) {
            return res.status(401).send({error: 'Token is not found'});
        }

        const user = await User.findOne({token: token});

        if (!user) {
            return res.status(401).send({error: 'User is not authorized'});
        }

        itemData.user = user._id;

        if (req.file) {
            itemData.image = req.file.filename;
        } else {
            itemData.image = null;
        }

        const item = new Item(itemData);

        await item.save();

        return res.send(item);
    });

    router.get('/:id', (req, res) => {

        const id = req.params.id;

        Item.findOne({_id: id}).populate('category user')
            .then(result => res.send(result))
            .catch(() => res.sendStatus(500));
    });

    router.delete('/:id', async (req, res) => {
        const token = req.get('Token');

        if (!token) res.status(401).send({error: 'Token is not present!'});

        const user = await User.findOne({token: token});

        if (!user) res.status(401).send({error: 'User not found!'});

        await Item.deleteOne({_id: req.params.id})
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error))
    });

    return router;
};

module.exports = createRouter;