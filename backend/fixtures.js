const mongoose = require('mongoose');
const config = require('./config');

const Category = require('./models/Category');
const Item = require('./models/Item');
const User = require('./models/User');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
    try {
        await db.dropCollection('users');
        await db.dropCollection('categories');
        await db.dropCollection('items');
    } catch (e) {
        console.log('Collections were not present, skipping drop...');
    }

    const [computerCategory, carCategory, otherCategory] = await Category.create({
        name: 'Computers'
    }, {
        name: 'Cars'
    }, {
        name: 'Other'
    });

    const user = await User.create({
        username: 'Mike',
        password: '123',
        displayName: 'Mike',
        phoneNumber: '654894313'
    });

    await Item.create({
        title: 'Asus',
        price: 30000,
        description: 'AMD 4Core A10-8700P',
        category: computerCategory._id,
        image: 'asus.jpg',
        user: user._id
    }, {
        title: 'Subaru',
        price: 400000,
        description: 'Subaru WRX STI',
        category: carCategory._id,
        image: 'subaru.jpg',
        user: user._id
    }, {
        title: 'Headphone',
        price: 10000,
        description: 'Gaming super headphone!',
        category: otherCategory._id,
        image: 'headphone.jpg',
        user: user._id
    });

    db.close();
});