import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import ItemList from "../../components/ItemList/ItemList";
import {fetchCategories, fetchItems} from "../../store/actions/items";
import {Link} from "react-router-dom";
import {ListGroup, ListGroupItem} from "react-bootstrap";

class Items extends Component {
    componentDidMount() {
        this.props.fetchItems();
        this.props.fetchCategories();
    }

    render() {
        return (
            <Fragment>
                <ListGroup>
                    <ListGroupItem><Link to={'/'}>All items</Link></ListGroupItem>
                    {this.props.categories.map(category => (
                        <ListGroupItem key={category._id}><Link to={`${category.name}`}>{category.name}</Link></ListGroupItem>
                    ))}
                </ListGroup>
                {this.props.items.map(item => (
                    <ItemList key={item._id} id={item._id} title={item.title} image={item.image} price={item.price}/>
                ))}
            </Fragment>
        )
    }
}

const mapStateToProps = state => ({
    items: state.items.items,
    categories: state.items.categories
});

const mapDispatchToProps = dispatch => ({
    fetchItems: () => dispatch(fetchItems()),
    fetchCategories: () => dispatch(fetchCategories())
});

export default connect(mapStateToProps, mapDispatchToProps)(Items);
