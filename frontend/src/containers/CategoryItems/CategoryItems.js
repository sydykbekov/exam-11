import React, {Component, Fragment} from 'react';
import ItemList from "../../components/ItemList/ItemList";
import {connect} from "react-redux";
import {fetchCategories, fetchThisCategoryItems} from "../../store/actions/items";
import {ListGroup, ListGroupItem} from "react-bootstrap";
import {Link} from "react-router-dom";


class CategoryItems extends Component {
    componentDidMount() {
        this.props.fetchCategories();
        this.props.fetchItems(this.props.match.params.category);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.match.params.category !== this.props.match.params.category) {
            this.props.fetchItems(this.props.match.params.category);
        }
    }

    render() {
        return (
            <Fragment>
                <ListGroup>
                    <ListGroupItem><Link to={'/'}>All items</Link></ListGroupItem>
                    {this.props.categories.map(category => (
                        <ListGroupItem key={category._id}><Link to={`${category.name}`}>{category.name}</Link></ListGroupItem>
                    ))}
                </ListGroup>
                {this.props.items.map(item => (
                    <ItemList key={item._id} id={item._id} title={item.title} image={item.image} price={item.price}/>
                ))}
            </Fragment>
        )
    }
}

const mapStateToProps = state => ({
    items: state.items.items,
    categories: state.items.categories
});

const mapDispatchToProps = dispatch => ({
    fetchItems: category => dispatch(fetchThisCategoryItems(category)),
    fetchCategories: () => dispatch(fetchCategories())
});


export default connect(mapStateToProps, mapDispatchToProps)(CategoryItems);