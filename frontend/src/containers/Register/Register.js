import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Button, Col, Form, FormGroup, PageHeader} from "react-bootstrap";
import FormElement from "../../components/UI/Form/FormElement";
import {registerUser} from "../../store/actions/users";

class Register extends Component {
    state = {
        username: '',
        password: '',
        displayName: '',
        phoneNumber: ''
    };

    inputChangeHandler = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    submitFormHandler = (event) => {
        event.preventDefault();

        this.props.registerUser(this.state);
    };

    render() {
        return (
            <Fragment>
                <PageHeader>Register new user</PageHeader>
                <Form horizontal onSubmit={this.submitFormHandler}>
                    <FormElement
                        propertyName="username"
                        title="Username"
                        placeholder="Enter username"
                        autoComplete="new-username"
                        type="text"
                        value={this.state.username}
                        changeHandler={this.inputChangeHandler}
                        error={(this.props.error && this.props.error.errors.username) && this.props.error.errors.username.message}
                    />

                    <FormElement
                        propertyName="password"
                        title="Password"
                        placeholder="Enter password"
                        autoComplete="new-password"
                        type="password"
                        value={this.state.password}
                        changeHandler={this.inputChangeHandler}
                        error={(this.props.error && this.props.error.errors.password) && this.props.error.errors.password.message}
                    />

                    <FormElement
                        propertyName="displayName"
                        title="Display name"
                        placeholder="Enter display name"
                        autoComplete="new-display-name"
                        type="text"
                        value={this.state.displayName}
                        changeHandler={this.inputChangeHandler}
                        error={(this.props.error && this.props.error.errors.displayName) && 'Enter your display name!'}
                    />

                    <FormElement
                        propertyName="phoneNumber"
                        title="Phone number"
                        placeholder="Enter phone number"
                        autoComplete="new-phone-number"
                        type="text"
                        value={this.state.phoneNumber}
                        changeHandler={this.inputChangeHandler}
                        error={(this.props.error && this.props.error.errors.phoneNumber) && 'Enter your phone number!'}
                    />

                    <FormGroup>
                        <Col smOffset={2} sm={10}>
                            <Button
                                bsStyle="primary"
                                type="submit"
                            >Register</Button>
                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    error: state.users.registerError
});

const mapDispatchToProps = dispatch => ({
    registerUser: (userData) => dispatch(registerUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);