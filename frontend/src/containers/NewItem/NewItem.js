import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Button, Col, ControlLabel, Form, FormControl, FormGroup, PageHeader} from "react-bootstrap";
import FormElement from "../../components/UI/Form/FormElement";
import {createItem, fetchCategories} from "../../store/actions/items";

class Register extends Component {
    state = {
        title: '',
        description: '',
        category: '',
        price: ''
    };

    componentDidMount() {
        this.props.fetchCategories();
    }

    inputChangeHandler = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();
        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key])
        });

        this.props.onSubmit(formData, this.props.user.token);
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };

    render() {
        return (
            <Fragment>
                <PageHeader>Register new user</PageHeader>
                <Form horizontal onSubmit={this.submitFormHandler}>
                    <FormElement
                        propertyName="title"
                        title="Title"
                        placeholder="title"
                        autoComplete="new-title"
                        type="text"
                        value={this.state.title}
                        changeHandler={this.inputChangeHandler}
                        required
                    />

                    <FormElement
                        propertyName="description"
                        title="Description"
                        placeholder="description ..."
                        autoComplete="new-description"
                        type="text"
                        value={this.state.description}
                        changeHandler={this.inputChangeHandler}
                        required
                    />

                    <FormElement
                        propertyName="price"
                        title="Price"
                        placeholder="price"
                        autoComplete="new-price"
                        type="number"
                        value={this.state.price}
                        changeHandler={this.inputChangeHandler}
                        required
                    />

                    <FormElement
                        propertyName="image"
                        title="Image"
                        type="file"
                        changeHandler={this.fileChangeHandler}
                    />

                    <FormGroup controlId="formControlsSelect">
                        <ControlLabel>Category</ControlLabel>
                        <FormControl name="category" componentClass="select" placeholder="select"
                                     onChange={this.inputChangeHandler}
                                     value={this.state.category}>
                            <option value="defaultValue">Select category</option>
                            {this.props.categories.map(category => (
                                <option key={category._id} value={category._id}>{category.name}</option>
                            ))}
                        </FormControl>
                    </FormGroup>

                    <FormGroup>
                        <Col smOffset={2} sm={10}>
                            <Button
                                bsStyle="primary"
                                type="submit"
                            >Create item</Button>
                        </Col>
                    </FormGroup>
                </Form>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    categories: state.items.categories,
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    onSubmit: (formData, token) => dispatch(createItem(formData, token)),
    fetchCategories: () => dispatch(fetchCategories())
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);