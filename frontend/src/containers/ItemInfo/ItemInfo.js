import React, {Component, Fragment} from 'react';
import {Button, Image, Panel} from "react-bootstrap";
import {connect} from "react-redux";
import {deleteItem, getInfo} from "../../store/actions/items";

class ItemInfo extends Component {
    componentDidMount() {
        this.props.getInfo(this.props.match.params.id);
    }

    render() {
        let button;
        if ((this.props.user && this.props.item) && (this.props.user._id === this.props.item.user._id)) {
            button = <Button bsStyle="danger" onClick={() => this.props.deleteItem(this.props.item._id, this.props.user.token)}>Danger</Button>;
        }
        let content;
        if (this.props.item) {
            content = (
                <Panel>
                    <Panel.Body>
                        <h3>{this.props.item.title}</h3>
                        <Image
                            style={{width: '100px', marginRight: '10px'}}
                            src={'http://localhost:8000/uploads/' + this.props.item.image}
                            thumbnail
                        />
                        <p>{this.props.item.price} KGS</p>
                        <p>{this.props.item.description}</p>
                        <p>{this.props.item.category.name}</p>
                        <p>{this.props.item.user.displayName}</p>
                        <p>{this.props.item.user.phoneNumber}</p>

                        {button}
                    </Panel.Body>
                </Panel>
            )
        } else {
            content = <div>Loading ...</div>
        }

        return (
            <Fragment>
                {content}
            </Fragment>
        )
    }
}

const mapStateToProps = state => ({
    item: state.items.item,
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    getInfo: id => dispatch(getInfo(id)),
    deleteItem: (id, token) => dispatch(deleteItem(id, token))
});

export default connect(mapStateToProps, mapDispatchToProps)(ItemInfo);