import React from 'react';
import {Image, Panel} from "react-bootstrap";
import {Link} from "react-router-dom";

const ItemList = props => {

    return (
        <Panel>
            <Panel.Body>
                <Image
                    style={{width: '100px', marginRight: '10px'}}
                    src={'http://localhost:8000/uploads/' + props.image}
                    thumbnail
                />
                <Link to={`/item/${props.id}`}>
                    {props.title} {props.price} KGS
                </Link>
            </Panel.Body>
        </Panel>
    )
};

export default ItemList;
