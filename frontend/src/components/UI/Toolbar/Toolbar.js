import React from 'react';
import {Nav, Navbar, NavItem} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";
import AnonymousMenu from "./Menus/AnonymousMenu";
import UserMenu from "./Menus/UserMenu";

const Toolbar = ({user, logout}) => (
    <Navbar>
        <Navbar.Header>
            <Navbar.Brand>
                <LinkContainer to="/" exact><a>Market</a></LinkContainer>
            </Navbar.Brand>
            <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
            <Nav>
                <LinkContainer to="/" exact>
                    <NavItem>Items</NavItem>
                </LinkContainer>
            </Nav>
            {user ? <UserMenu user={user} logout={logout} /> : <AnonymousMenu/>}
        </Navbar.Collapse>
    </Navbar>
);

export default Toolbar;