import React, {Component} from 'react';
import './App.css';
import {Route, Switch} from "react-router-dom";
import Items from "./containers/Items/Items";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Layout from "./containers/Layout/Layout";
import NewItem from "./containers/NewItem/NewItem";
import CategoryItems from "./containers/CategoryItems/CategoryItems";
import ItemInfo from "./containers/ItemInfo/ItemInfo";

class App extends Component {
    render() {
        return (
            <Layout>
                <Switch>
                    <Route path="/" exact component={Items}/>
                    <Route path="/register" exact component={Register}/>
                    <Route path="/login" exact component={Login}/>
                    <Route path="/create" exact component={NewItem}/>
                    <Route path="/:category" exact component={CategoryItems}/>
                    <Route path="/item/:id" exact component={ItemInfo}/>
                </Switch>
            </Layout>
        );
    }
}

export default App;
