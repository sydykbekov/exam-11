import {
    FETCH_CATEGORIES_SUCCESS, FETCH_CHECKED_CATEGORY_ITEMS, FETCH_ITEMS_SUCCESS,
    GET_INFO_SUCCESS
} from "../actions/items";

const initialState = {
    items: [],
    categories: [],
    item: null
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ITEMS_SUCCESS:
            return {...state, items: action.items};
        case FETCH_CATEGORIES_SUCCESS:
            return {...state, categories: action.categories};
        case FETCH_CHECKED_CATEGORY_ITEMS:
            return {...state, items: action.items};
        case GET_INFO_SUCCESS:
            return {...state, item: action.item};
        default:
            return state;
    }
};

export default reducer;