import axios from 'axios';
import {push} from 'react-router-redux';
export const FETCH_ITEMS_SUCCESS = 'FETCH_ITEMS_SUCCESS';
export const FETCH_CATEGORIES_SUCCESS = 'FETCH_CATEGORIES_SUCCESS';
export const FETCH_CHECKED_CATEGORY_ITEMS = 'FETCH_CHECKED_CATEGORY_ITEMS';
export const GET_INFO_SUCCESS = 'GET_INFO_SUCCESS';

const fetchItemsSuccess = items => {
    return {type: FETCH_ITEMS_SUCCESS, items}
};

const fetchCategoriesSuccess = categories => {
    return {type: FETCH_CATEGORIES_SUCCESS, categories};
};

const fetchCheckedCategoryItems = items => {
    return {type: FETCH_CHECKED_CATEGORY_ITEMS, items};
};

const getInfoSuccess = item => {
    return {type: GET_INFO_SUCCESS, item};
};

export const fetchItems = () => {
    return dispatch => {
        axios.get('items').then(response => {
            dispatch(fetchItemsSuccess(response.data));
        })
    }
};

export const fetchCategories = () => {
    return dispatch => {
        axios.get('categories').then(response => {
            dispatch(fetchCategoriesSuccess(response.data));
        })
    }
};

export const fetchThisCategoryItems = category => {
    return dispatch => {
        axios.get(`items?category=${category}`).then(response => {
            dispatch(fetchCheckedCategoryItems(response.data));
        })
    }
};

export const getInfo = id => {
    return dispatch => {
        axios.get(`items/${id}`).then(response => {
            dispatch(getInfoSuccess(response.data));
        })
    }
};

export const deleteItem = (id, token) => {
    return dispatch => {
        const headers = {"Token": token};
        axios.delete(`items/${id}`, {headers}).then(() => {
            dispatch(push('/'));
        })
    }
};

export const createItem = (formData, token) => {
    return dispatch => {
        const headers = {"Token": token};
        axios.post('items', formData, {headers}).then(() => {
            dispatch(push('/'));
        })
    }
};